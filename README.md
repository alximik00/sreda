# Sreda interior site

This is a site for 'Sreda' interior design company.

**node.js** is required for this to work.
The basic idea for structure of this site is following:
I wanted to have full node-js functionality (less, templates, database etc.), but also I would like this site to be served as static content.

So, I did following:
* Wrote base site with express.js
* Wrote grabber in node.js that dumps whole site as static html/js/css files

That static content can be served by any simple web-server (nginx,apache, etc.) that can rewrire requests from `http://domain/path/to/something` to `http://domain/path/to/something/index.html`.

App that grabs a site lives in `gen-static` folder, to execute it run `cd gen-static && node crawler.js`. It has hardcoded path references to output folder and source images folder, so be aware. After running dumper static will be under `dist` folder. So, in most basic case, you can install nodejs-based basic http server named (unexptectedly) *http-server* (can be installed by running `npm install http-server -g`, github: [https://github.com/indexzero/http-server](https://github.com/indexzero/http-server) ). It's run easily by `http-server path/to/dist/folder -p port` (for example, `http server ./dist -p 3001`).

App follows any `<a href='something'>` links to discover pages behind root, also dumps needed **.js** and **.css** files.

**Notice**: grabbing app actually wouldn't work correctly for any site, cause it doesn't capture images from page itself (I was too lazy to parse css and html for background images).

**Notice 2**: before grabbing site to static, you have to manually start primary express like that: `NODE_ENV=production npm start` (it will disable live-reload plugin that produces additional javascript for client-side).
