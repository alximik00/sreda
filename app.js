var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();

global.appRoot = path.resolve(__dirname);

var nodeEnv = process.env.NODE_ENV || 'development';
var expenv = {development: nodeEnv == 'development',
              production: nodeEnv == 'production'};

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

var routes = require('./routes/index');

app.use(function(req,res,next){
  var p = req.path;
  res.locals.path = req.path;
  res.locals.bodyClass = req.path.split('/')[1] || 'home';

  res.locals.isActivePage = function(path) {
    if (p == '/' && path == 'index') {
      return true;
    }
    if (p.startsWith(`/${path}`)) {
      return true;
    }
    return false;
  };

  next();
});

app.use('/', routes);
app.use('/users', require('./routes/users'));
app.use('/portfolio', require('./routes/portfolio/index'));
app.use('/services', require('./routes/services/index'));
app.use('/team', require('./routes/team/index'));
app.use('/contacts', require('./routes/contacts/index'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    console.error(err.message);
    console.error(err);

    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

if (expenv.development) {
  var livereload = require('./lib/livereload/express-livereload');
  livereload(app, {
    watchDir: [path.join(__dirname, 'views'), path.join(__dirname, 'public')],
    exts: ['jade', 'less', 'js']
  });
  console.log("Live reloading should work now");
}

module.exports = app;