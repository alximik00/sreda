'use strict';

const jsdom = require('jsdom');
const http = require('http');
const fs = require('fs');
const path = require('path');
const urljoin = require('url-join');
const mkdirp = require('mkdirp');
const url = require('url');
const Q = require('q');
const ncp = require('ncp').ncp;

const imagesDir = path.join(__dirname, '../public/images');


// configure jsdom
jsdom.defaultDocumentFeatures = {
  FetchExternalResources   : ['script', 'link'],
  ProcessExternalResources : ['script'],
  MutationEvents           : '2.0',
  QuerySelector            : false
};

// initial setip
const distDir = path.join(__dirname,  '../dist');
const baseUrl = "http://localhost:3002";
var pagePath = "/";
const exclude = ["/services", '/groups'];

// storage
const visited = [];
const resDumped = [];

function toArray(collection) {
  return Array.prototype.slice.call(collection);
}

Array.prototype.includes = function(item) {
  return this.indexOf(item) >= 0;
};

function dumpResource(url) {
  resDumped.push(url);
  var scriptDir = path.dirname(path.join(distDir, url));
  mkdirp.sync(scriptDir);

  var file = fs.createWriteStream(path.join(distDir, url));
  var request = http.get( urljoin(baseUrl, url), function(response) {
    response.pipe(file);
  });
}

function writeHtmlFile(pagePath, text) {
  const filePath = path.join(distDir, pagePath.replace(/^\//,''), 'index.html');
  const htmlDir = path.dirname(filePath);
  mkdirp.sync(htmlDir);

  const file = fs.createWriteStream( filePath);
  file.write(text);
  file.end();
}

function dumpPage(pagePath, html, window) {
  var scripts = toArray( window.document.getElementsByTagName('script') ).map((x) => { return x.src; });
  var stylesheets = toArray( window.document.getElementsByTagName('link') ).filter( x=> x.rel == 'stylesheet').map( x=>x.href);

  writeHtmlFile(pagePath, html);
  scripts.
      concat(stylesheets).
      filter((v) => { return v.startsWith('http://localhost') || v.startsWith('/'); }).
      map( (x) => {
        var resPath = x.replace(/http:\/\/localhost(:\d+)?/, '');
        if ( !resDumped.includes(resPath)) {
          dumpResource(resPath);
        }
      });
}


function copyImages() {
  let destImagesDir = path.join(distDir, '/images');
  mkdirp.sync( destImagesDir) ;
  let srcImagesDir = path.join(__dirname, '../public/images/') ;
  ncp(srcImagesDir, destImagesDir);
  ncp( path.join(__dirname, '../public/favicon.png') , path.join(distDir, 'favicon.png'));
}

function dumpUrl(pagePath) {
  const defer = Q.defer();

  if (exclude.find( function(el) { return pagePath.startsWith(el) } ) ) {
    defer.resolve(pagePath);
    return defer.promise;
  }

  if (visited.includes(pagePath)) {
    defer.resolve(pagePath);
    return defer.promise;
  }
  console.log(`Started ${pagePath}`);
  visited.push(pagePath);

  jsdom.env({
    url: urljoin(baseUrl, pagePath),
    done: (err, window)=> {
      if (err) {
        console.error("Error happened: " + err);
        defer.reject(err);
      } else {
        var html = window.document.documentElement.outerHTML;
        dumpPage(pagePath, html, window);

        var references = toArray( window.document.getElementsByTagName('a') )
        .filter(x=> !visited.includes(x) )
        .map( x=> {
          let aHref = url.parse(x.href);
          return aHref.pathname || '/';
        } );

        var all = Q.allSettled( references.map( refPath => Q.fcall(dumpUrl, refPath) ) );
        return defer.resolve( all );
      }
    }
  });
  return defer.promise;
}


let baseUrls = ['/'];
Q.allSettled( baseUrls.map(x=>dumpUrl(x) ) )
.catch( err => console.log(`Error happened - ${err}`) )
.done((res, err) => {
  copyImages();
  console.log('Done');
});
// result can be served with https://github.com/indexzero/http-server
