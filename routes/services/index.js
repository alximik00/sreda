"use strict";

var express = require('express');
var router = express.Router();
var sha1 = require('sha1');

var sendEmail = require('../../lib/mail');

/* GET home page. */
router.get('/', function(req, res, next) {

  res.render('services/index', { title: 'Sreda - Услуги' });
});

router.post('/', function(req, res, next) {

  var phone = req.body['phone'];
  var name = req.body['name'];
  var style = req.body['style'] == 'classic' ? 'классический' : 'современный';
  var type = req.body['type'] == 'public' ? 'коммерческий' : 'частный';
  var area = req.body['area'];
  var steps = [];
  if (req.body['design'] == 'true')     steps.push('коцепт-дизайн');
  if (req.body['drawings'] == 'true')   steps.push('пакет чертежей');
  if (req.body['management'] == 'true') steps.push('ведение проекта');

  var text = `
${name} с телефоном ${phone} хочет сделать проект со следующими характеристиками:
 * Площадь - ${area}
 * Тип - ${type}
 * Стиль - ${style}
 * Этапы - ${steps.join(', ')}
`;
  console.log("Sending email with text: \n" + text);

  sendEmail('sreda_ds@mail.ua', 'notification@sreda.in.ua', "Новый заказ!", text);
  res.status(200).send('ok');//render('services/index', { title: 'Sreda - Услуги' });
});

module.exports = router;
