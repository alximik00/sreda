const data = [
  {
    "folder": "1",
    "title": "В сером цвете. Квартира 100м.кв",
    "count": 21,
    "tile": "big",
    "mnemonic": "apartments/gray"
  },
  {
    "tile": "small_text",
    "text": "Дома и квартиры"
  },
  {
    "folder": "2",
    "title": "Винтажная. Квартира 96м.кв",
    "count": 6,
    "tile": "small",
    "mnemonic": "apartments/vintage"
  },
  {
    "folder": "3",
    "title": "Квартира холостяка. Квартира 75 м.кв",
    "count": 8,
    "tile": "vertical",
    "mnemonic": "apartments/loner"
  },
  {
    "folder": "4",
    "title": "Мужской интерьер. Квартира 78м.кв",
    "count": 9,
    "tile": "horizontal",
    "mnemonic": "apartments/for_man"
  },
  {
    "folder": "5",
    "title": "Светлая квартира. Квартира 74м.кв",
    "count": 27,
    "tile": "horizontal",
    "mnemonic": "apartments/light"
  },
  {
    "folder": "6",
    "title": "Яркая идея.Квартира 74м.кв",
    "count": 18,
    "tile": "horizontal",
    "mnemonic": "apartments/bright_idea"
  },
  {
    "folder": "7",
    "tile": "horizontal",
    "count": 6,
    "title": "Уютный дом. Дом 120м.кв",
    "mnemonic": "apartments/cosy_house"
  },


  {
    "tile": "small_text",
    "text": "Для бизнеса"
  },
  {
    "tile": "small",
    "title": "Магазин мужской одежды «Тамир»",
    "folder": "8-business",
    "count": 13,
    "mnemonic": "commercial/tamir"
  },

  {
    "tile": "small",
    "title": "Мебель",
    "folder": "9-furniture",
    "count": 13,
    "mnemonic": "commercial/furniture"
  },
  {
    "tile": "small_text",
    "text": "Мебель"
  }
];

module.exports = data;