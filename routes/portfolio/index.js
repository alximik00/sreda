"use strict";

const express = require('express');
const router = express.Router();
const fs = require('fs');

function checkUniqueMnemonics(res, pages) {
  var set = new Set();
  for(let item of pages) {
    if (item.mnemonic && set.has(item.mnemonic) ) {
      var message = "Not unique mnemonic '" + item.mnemonic + "'";
      console.log(message);
      res.status(500).send(message);
      return false;
    }
    set.add(item.mnemonic);
  }
  return true;
}

function loadPagesData(){
  var data = require('./portfolio_data');
  return data;
}

function findPage(pages, mnemonic) {
  for(let item of pages) {
    if (item.mnemonic == mnemonic) {
      return item;
    }
  }
  return null;
}

/* GET home page. */
router.get('/', function(req, res, next) {

  var pages = loadPagesData();

  if (!checkUniqueMnemonics(res, pages)) {
    return;
  }

  var startPair = false;

  pages.forEach( (item) => {
    if (item.tile == "small_text") {
      item.className = "width1 height1";
      item.startPair = startPair = !startPair;

    } else if (item.tile == "big") {
      item.className = "width2 height2";
      startPair = false;

    } else if (item.tile == "horizontal") {
      item.className = "width2 height1";
      item.startPair = startPair = !startPair;

    } else if (item.tile == "vertical") {
      item.className = "width1 height2";
      startPair = false;

    } else {
      item.className = "width1 height1";
      item.startPair = startPair = !startPair;

    }
  });


  res.render('portfolio/index', { title: 'Sreda - Портфолио', pages: pages });
});

router.get('/*', function(req, res, next) {
  var mnemonic = req.path.replace(/^\//, '');
  var pages = loadPagesData();
  var page = findPage(pages, mnemonic);

  console.log('Current path = ' + mnemonic + ", page found: " + page);


  res.render('portfolio/project', { title: 'Sreda - Проект: ' + page.title , page: page });
});

module.exports = router;
