#!/usr/bin/env bash
PORT=3002 NODE_ENV=production npm start &
sleep 3
cd gen-static
node crawler.js
kill %1
cd ..
git add dist
git commit -m "static copy created"
git push