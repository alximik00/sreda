"use strict";

function calculateCost(type, style, area, design, drawings, management) {

  area = Number.parseFloat(area);

  var total = ( 1.0 + type + style ) * area;

  var cost = total;
  var third = total / 3;
  if (!design) cost -=third;
  if (!drawings) cost -=third;
  if (!management) cost -=third;

  cost *= 20;

  return Math.round(cost/10)*10;
}


$(function() {
  // dont run on other pages
  if (!$('.calc-area').length) {
    return;
  }

  // helper functions
  function formatResult(value) {
    if (Number.isNaN(value)) {
      $('#result').html('Пожалуйста, введите площадь');
    } else {
      $('#result').html('$' + value);
    }
  }

  function recalc() {
    var type = $('.calc-area .active input[name=group-type]').val();
    var style = $('.calc-area .active input[name=group-style]').val();
    var area = $('#input-area').val();

    var design = $('#btn-art').hasClass('active');
    var drawings = $('#btn-drawings').hasClass('active');
    var management = $('#btn-management').hasClass('active');


    design = design ? 1 : 0;
    drawings = drawings ? 1 : 0;
    management = management ? 1 : 0;

    type = (type == 'public') ?  - 0.25 : 0;
    style = (style == 'classic') ? 0.3 :  0;

    var value = calculateCost(type, style, area, design, drawings, management);
    formatResult(value);
  }

  function fireRecalc() {
    setTimeout( recalc, 200);
  }

  function inputAreaChanged() {
    var value = $('#input-area').val();
    value = Number.parseFloat(value);
    $('#input-area-slider').slider('setValue', value);
    fireRecalc();
  }

  // setup plugins
  $('#input-area-slider').slider();
  $('#phone').mask('+38999-999-99-99', {placeholder: '_'});

  // setup events
  $('.calc-area input[type=checkbox]').change(fireRecalc);
  $('.calc-area input[type=radio]').change(fireRecalc);
  $('.calc-area button').click(fireRecalc);

  $('#input-area')
    .on('change', inputAreaChanged)
    .on('input', inputAreaChanged);

  $('#input-area-slider').on('slide', function () {
    $('#input-area').val( $(this).val() ).trigger('change');
  });


  // send form
  $('#btn-order').click(function () {
    var phone = $('#phone').val();
    if (phone.length < 16) {
      alert("Пожалуйста, введите телефон для обратной связи");
      return;
    }

    var type = $('.calc-area .active input[name=group-type]').val();
    var style = $('.calc-area .active input[name=group-style]').val();
    var area = $('#input-area').val();
    var name = $('#name').val();

    var design = $('#btn-art').hasClass('active');
    var drawings = $('#btn-drawings').hasClass('active');
    var management = $('#btn-management').hasClass('active');

    var data = {
      phone: phone,
      type: type,
      style: style,
      area: area,
      design: design,
      drawings: drawings,
      management: management,
      name: name
    };
    var $btn = $(this).button('loading');

    $.ajax({
      url: '/services/',
      method: 'POST',
      data: data,
      success: function () {
        $('.order-block').hide();
        var alert = $('.alert-success').show();
        // setTimeout(function () {
        //   alert.fadeOut();
        // },4000);

        $btn.button('reset');
      },
      error: function () {
        $btn.button('reset');
      }
    });
  });

  recalc();
});