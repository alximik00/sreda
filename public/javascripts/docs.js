"use strict";

$(function(){
  var backLink = $('.back-link');
  if (backLink.length) {
    backLink.hide();
    $(window).scroll(function() {
      var top = document.body.scrollTop;
      if (top != 0 && backLink.css('display') == 'none') {
        backLink.fadeIn();
      }
    });

    backLink.find('a').click(function() {
      backLink.fadeOut();
    })
  }
});